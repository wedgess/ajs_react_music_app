## Installation
```sh
git clone https://bitbucket.org/wedgess/ajs_react_music_app && cd ajs_react_music_app && npm install && npm start
```

## Live Demo
--------------------------------------------------
#### The live demo is hosted on firebase and can be found [here](https://react-music-app.firebaseapp.com)



# React CA Report - Music Lyrics
--------------------

## User Stories
- As a music lover, I want to be able to view a track's lyrics so that I can sing along with the song.
- As a music lover, I want to be able to view an album's information so that I can see which tracks are on the album.
- As a music lover, I want to be able to see which tracks contain explicit lyrics so that I can avoid playing these songs around children.
- As a music lover, I want to be able to search for artists so that I can see their albums and top tracks.
- As a music lover, I want to be able to search for albums and tracks so that I can quickly find what I’m looking for.
- As a music lover, I want to be able to favourite albums and tracks so that I can quickly find my favourites.



### Charts Page
![Charts Page](https://i.imgur.com/KAFlkIYl.png)

The charts page is the websites home page that displays the top ten albums and top ten tracks according to Deezer. Upon clicking one of the top albums the user is brought to the album information page. Clicking a top track will bring the user to the track information page.


### Album Information
![AlbumInfo Page](https://i.imgur.com/6fpeiRfl.png)

The album information page displays information about the album, including a list of tracks present on the album. Each track has two buttons, one button plays a thirty second preview of the track while the second button opens a modal window which displays the track lyrics.


### Track Information
![TrackInfo Page](https://i.imgur.com/TjgqYzsl.png)

The track info page displays information about a specific track, such as duration, whether it contains explicit lyrics, the artist's name, the track's lyrics and a thirty second preview of the track. In implementing the audio, a package called [react-audio-player](https://github.com/justinmc/react-audio-player) was used. This page is only accessible by clicking on one of the top ten tracks from the charts page.


### Artist Info page
![ArtistInfo Page](https://i.imgur.com/8W3SxNHl.png)

The artist information page displays information about a specific artist. This page is accessible through the search page by searching for a specific artist. The information provided in this page is the top five tracks and a list of each album by the artist. Clicking on one of the artist's albums brings the user to the album information page.


### Search Page
![Search Page](https://i.imgur.com/VwfSIsNl.png)

The search page allows users to search for artists, albums or tracks. Clicking on an album item brings the user to the album information page. Clicking on a track item brings the user to the track information page and clicking on an artist item brings the user to the artist information page.


### Profile Page
![Profile Page](https://i.imgur.com/234WHwJl.png)

The profile page contains the account information which the user registered with. There is a password reset option that allows the user to change their current password. The user can access the list of their favourited items from this page, giving them the option to view, delete and search their favourited items.


### SignIn Page
![SignInPage](https://i.imgur.com/2l8tohDl.png)

The sign in page allows the user to sign into their firebase account to which favourited items are associated. In case the user does not already have an account there is a link that allows the user to register. There is also a forgotten password link in this page.


### PasswordReset Page
![PasswordResetPage](https://i.imgur.com/guMN92il.png)

This page is accessed by clicking the forgotten password link in the sign in page. If the user has forgotten their password, this form allows the user to send a temporary password to the email they registered with.


### SignUp Page
![SignUpPage](https://i.imgur.com/1p1hWEvl.png)

The sign-up page is where the user can register an account for the application.



## Requirements
---------------------------

#### Components
Components were used to make the code modular, a lot of these components are re-used throughout the application. For example, the *CardListItem* component is used to display each of the items used in the charts page, the search page and the artists information page. Components are exported so that they can be imported and used by other components.



#### Props & Typechecking
Props were used throughout the application. In most cases props were passed down from the parent or calling component. For the favourites search, a function is passed down from the parent component ([ProfilePage](https://bitbucket.org/wedgess/ajs_react_music_app/src/b5eb41cc227315abdc47ad129548850a14dbf636/src/components/pages/Profile.js?at=master&fileviewer=file-view-default#Profile.js-29)) to the child component ([FavouritesSearchForm](https://bitbucket.org/wedgess/ajs_react_music_app/src/b5eb41cc227315abdc47ad129548850a14dbf636/src/components/lists/FavouritesList.js?at=master&fileviewer=file-view-default#FavouritesList.js-31)) as a prop which is then used as a callback function for when the forms input fields change. All props for each component were type-checked using the [prop-types](https://www.npmjs.com/package/prop-types) library. Using the prop-types library also allowed for certain props to be marked as required as can be seen in the example below.


###### Part of the [InformationComponent.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/components/InformationComponent.js?at=master&fileviewer=file-view-default)
```js
InformationComponent.propTypes = {
  cover: PropTypes.string.isRequired,
  artist_img: PropTypes.string.isRequired,
  track_time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  release_date: PropTypes.string,
  explicit_lyrics: PropTypes.bool,
};
```


#### Router
React router was used to make this application a single paged application. The BrowserRouter component was used in the *App.js* file as the application serves dynamic web pages. Each of the applications routes are then defined within the BrowserRouter component. Some of these Routes contain the *[component](https://reacttraining.com/react-router/web/api/Route/component)* prop while others contain the *[render](https://reacttraining.com/react-router/web/api/Route/render-func)* prop. Render was used for inline rendering of components to which props were passed using the ```history.push``` function. A redirect was used to redirect users from the landing route *"/"* to the *"charts"* page. The redirect is required or the charts page remains the active link in the navigation bar even when on another page. An example of the routes used can be seen below.


###### Part of [App.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/App.js?at=master&fileviewer=file-view-default)
```js
<Route
  exact
  path={routes.LANDING}
  render={() => <Redirect to={routes.CHARTS} />}
/>
<Route exact path={routes.CHARTS} component={ChartsPage} />
<Route path={routes.ALBUM_CHART_ITEM} render={() => <AlbumInfo />} />
```


The *withRouter* higher order component was used to wrap certain components giving them access to the *history* prop provided by react-router. Using the push function of the history prop allowed for the new component to be rendered on the page and for props to be passed to the component. The example below shows the use of the history.push function, and shows how one of the dynamic urls is built. The applications routes or URLs are found in [routes.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/b5eb41cc227315abdc47ad129548850a14dbf636/src/constants/routes.js?at=master&fileviewer=file-view-default).


###### Part of [CardListItem.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/components/lists/items/CardListItem.js?at=master&fileviewer=file-view-default)
```js
onItemClick = event => {
  let prop = this.props.info;
  this.props.history.push({
    pathname: `${this.props.rootPage}/${this.props.type}/${prop.title
      .split(" ")
      .join("")}`,
    state: {
      detail: prop
    }
  });
};
```


#### External API's
The external API's used were [Deezer](https://developers.deezer.com/api) and [lyrics.ovh API](https://lyricsovh.docs.apiary.io/#reference/0/lyrics-of-a-song/search). Deezer was used to fetch information such as the charts, artist, album and track information. lyrics.ovh was used to fetch the lyrics for the tracks. When using the Deezer API there were errors fetching data due to [Cross-Origin-Resource-Sharing (CORS)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS), to fix this issue a package called [fetch-JSONP](https://www.npmjs.com/package/fetch-jsonp) was used. Fetch JSONP worked the same way as JavaScript's fetch but required ```&output=jsonp``` to be appended to the query URL. An example of using fetch-jsonp can be seen below:



###### Part of [ChartsListType.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/components/lists/ChartsListType.js?at=master&fileviewer=file-view-default)
```js
fetchJsonp(
  `https://api.deezer.com/chart/0/${this.props.type}s&output=jsonp`
)
  .then(response => {
    if (response.ok) return response.json();
    throw new Error("Request failed");
  })
  .then(data => {
    this.setState({
      charts: data.data
    });
  })
  .catch(error => {
    console.log(error);
  });
```



## Extras
---------------------------

#### Higher Order Components(#firebase-example-usage)
Two higher order components (HOC) were used. These were withAuthorization and withAuthentication. The withAuthorization HOC ensires that only authenticated users can view the profile page. The withAuthentication HOC was used to wrap components that needed access to check if the user was authenticated with [firebase](#firebase-example-usage). The withAuthentication HOC was also used to read the list of users favourited items. Checking if users were authenticated and controlling their favourites was handled in the HOCs componentDidMount lifecycle function as can be seen below:


###### Part of [withAuthentication.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/components/hocs/withAuthentication.js?at=master&fileviewer=file-view-default)
```js
componentDidMount() {
  // add FB listener for authentication change
  firebase.auth.onAuthStateChanged(authUser => {
    // when user is signed in
    if (authUser) {
      // get reference to user's favourites in FB DB
      let remoteFavs = firebase.db
        .ref(`users/${authUser.uid}`)
        .child("favourites");
      // add listener to remote to listen to db changes
      remoteFavs.on("value", snapshot => {
        let userFavourites = snapshot.val();
        // save favourites in state
        this.setState({ favourites: userFavourites });
      });
      // save authenticated user in state
      this.setState({ authUser: authUser });
    } else {
      // on sign out reset state
      this.setState({ ...INITIAL_STATE });
    }
  });
}
```


#### Context API
Within the withAuthentication HOC the context API was used so that both the favourited items and the authUser object could be saved to a "global state" that could be accessed by components which required these objects. Providing access to these objects through the Context API can be seen below.


###### Part of [withAuthentication.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/components/hocs/withAuthentication.js?at=master&fileviewer=file-view-default)
```js
getChildContext() {
  return {
    authUser: this.state.authUser,
    favourites: this.state.favourites
  };
}
```


Components that access the context must pass the context through their constructor as shown below.

###### Part of [CardListItem.js](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/components/lists/items/CardListItem.js?at=master&fileviewer=file-view-default)
```js
constructor(props, context) {
    super(props, context);
    // ....
}
```


#### Firebase
Firebase was used to allow the user to login and register an account. Using firebase authentication allowed for each user to add favourited items using firebase database. Firebase database is a cloud based NO-SQL database. Therefore, rather than data being stored in tables it is stored in nodes. The root node of the database is the *users* node with each child node being a registered user's id. The user id is automatically created by firebase when a user registers an account. Under each user node there are mutiple child nodes which are the email address that the user registered with, their username and a list of each of their favourited items. The id of the track or album is the key for the favourited item with the value being a JSON string of the album or track. The structure of the database can be seen below.

![Imgur](https://i.imgur.com/M3xVJ6nl.png)


The [HOC section](markdown-header-firebase-example-usage) above shows the usage of the authentication listener used for listening to when a user signs in or out. When a user is authenticated a child event listener is added to the user's favourites node in firebase database. As firebase database is a realtime database. When a favourited item is added or removed, the favourites state defined in the HOC will update automatically.


#### SASS
SASS was used to style the application with a CSS framework called [MaterializeCSS](http://materializecss.com/). MaterializeCSS styles elements to replicate Googles Material Design. There is also a [react-materialize](https://react-materialize.github.io/#/) library which was used to allow certain UI elements to be implemented as components such as NavBar, Row, Input, Collapsible and CollapisbleItems. The SASS files are compiled to CSS files by using package called [node-sass-chokidar](https://github.com/michaelwayman/node-sass-chokidar). In order for the SASS files to be compiled as CSS, chokidar must be added to the scripts object of the packages.json file. The watch-css key points to the directory to watch for file changes. This means that once a SASS file is changed the CSS file is re-built.

```js
"build-css": "node-sass-chokidar src/ -o src/",
"watch-css": "npm run build-css && node-sass-chokidar src/ -o src/ --watch --recursive",
"start-js": "react-scripts start",
"start": "npm-run-all -p watch-css start-js",
"build-js": "react-scripts build",
"build": "npm-run-all build-css build-js",
```


SASS mixins were written to provide media queries for both mobile and tablet screen sizes which allowed for the application to be displayed appropriately for desktop, tablet and mobile devices.


###### Part of [variables.scss](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/sass/variables.scss?at=master&fileviewer=file-view-default)
```sass
  $tablet-width: 768px;
  $desktop-width: 1024px;

  @mixin tablet {
    @media (min-width: #{$tablet-width}) and (max-width: #{$desktop-width - 1px}) {
      @content;
    }
  }
  @mixin mobile {
    @media (max-width: #{$mobile-width}) {
      @content;
    }
  }
```


Usage:
###### Part of [track-info.scss](https://bitbucket.org/wedgess/ajs_react_music_app/src/2a7d2cebdb3331f6a4ce882776adc0da7db6b0c2/src/sass/track-info.scss?at=master&fileviewer=file-view-default)
```sass
  @include mobile {
    th.track-duration-heading > i.material-icons {
      font-size: 1.4rem;
    }

    td.action-column {
      width: 140px !important;
    }

    div.card {
      padding-left: 16px;
      padding-right: 16px;
    }
  }
```


### References
- [React Firebase Authentication tutorial](https://www.robinwieruch.de/complete-firebase-authentication-react-tutorial/)
