import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./sass/base.css";

// App entry point
ReactDOM.render(<App />, document.getElementById("root"));
