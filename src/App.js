import React from "react";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import ChartsPage from "./components/pages/Charts";
import SearchPage from "./components/pages/Search";
import ProfilePage from "./components/pages/Profile";
import AlbumInfo from "./components/pages/AlbumInfo";
import TrackInfoPage from "./components/pages/TrackInfo";
import ArtistInfoPage from "./components/pages/ArtistInfo";
import withAuthentication from "./components/hocs/withAuthentication";
import PasswordResetPage from "./components/pages/PasswordReset";
import SignUpPage from "./components/pages/SignUp";
import SignIn from "./components/pages/SignIn";
import Navigation from "./components/Navigation";
import * as routes from "./constants/routes";
import "./sass/materialize.css";
import "./sass/base.css";

// App Component setting up all rountes and hosting nav bar, exact is needed on a number
// of routes as child views will refresh in page otherwise.
const App = () => (
  <div>
    <BrowserRouter>
      <span>
        <Navigation />
        <Route
          exact
          path={routes.LANDING}
          render={() => <Redirect to={routes.CHARTS} />}
        />
        <Route exact path={routes.CHARTS} component={ChartsPage} />
        <Route exact path={routes.SEARCH} component={SearchPage} />
        <Route exact path={routes.PROFILE} component={ProfilePage} />
        <Route path={routes.SIGN_IN} component={SignIn} />
        <Route path={routes.SIGN_UP} component={SignUpPage} />
        <Route path={routes.PASSWORD_RESET} component={PasswordResetPage} />
        <Route path={routes.ALBUM_CHART_ITEM} render={() => <AlbumInfo />} />
        <Route
          path={routes.TRACK_CHART_ITEM}
          render={() => <TrackInfoPage />}
        />
        <Route path={routes.TRACK_FAV_ITEM} render={() => <TrackInfoPage />} />
        <Route path={routes.ALBUM_FAV_ITEM} render={() => <AlbumInfo />} />
        <Route
          exact
          path={routes.SEARCH_ARTIST}
          render={() => <ArtistInfoPage />}
        />
        <Route path={routes.SEARCH_ALBUM} render={() => <AlbumInfo />} />
        <Route path={routes.SEARCH_TRACK} render={() => <TrackInfoPage />} />
        <Route path={routes.SEARCH_ARTIST_ALBUM} render={() => <AlbumInfo />} />
      </span>
    </BrowserRouter>
  </div>
);

// required to be wrapped by HOC to get auth token
export default withAuthentication(App);
