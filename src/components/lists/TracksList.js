import React from "react";
import PropTypes from "prop-types";
import TrackListItem from "./items/TrackListItem";

// Component to display the list of tracks for an album
const TracksList = props => {
  let list = props.tracks
    .sort((a, b) => a.track_position - b.track_position)
    .map((track, index) => {
      return (
        <TrackListItem key={index} track={track} artist={props.artistName} />
      );
    });
  return (
      <section className="card track-card">
        <table className="bordered">
          <thead>
            <tr>
              <th className="track-position">#</th>
              <th className="track-name">Name</th>
              <th />
              <th className="track-duration-heading">
                <i className="material-icons">schedule</i>
              </th>
              <th />
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </table>
      </section>
  );
};

// Typechecking
TracksList.propTypes = {
  tracks: PropTypes.array,
  artistName: PropTypes.string
};

export default TracksList;
