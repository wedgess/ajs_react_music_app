import React from "react";
import PropTypes from "prop-types";
import { Row, Input } from "react-materialize";
import FavouritedItem from "./items/FavouritedItem";
import OptionsItem from "./items/OptionsItem";

/*
 * Displays user's favourite albums/tracks.
 * Favourites list component is used by Account component.
 * Favourites search component is only user by favourites list.
 */

// Favourites search Input component
const FavouritesSearchForm = props => (
  <Row>
    <Input
      name="search"
      s={12}
      m={6}
      label="Search"
      value={props.search}
      onChange={props.inputChange}
    />
    <Input
      name="type"
      s={12}
      m={6}
      type="select"
      label="Type"
      value={props.type}
      onChange={props.inputChange}
    >
      <OptionsItem key={0} typeKey="all" typeValue="All" />
      <OptionsItem key={1} typeKey="album" typeValue="Album" />
      <OptionsItem key={2} typeKey="track" typeValue="Track" />
    </Input>
  </Row>
);

// component that displays a list of the user's favourite charts/albums
const FavouritesList = (props) => {
  // list of FavouritedItem components
  const favouriteItems = props.favourites
    .filter(
      item =>
        props.search === ""
          ? true
          : item.title
              .toLowerCase()
              .includes(props.search.toLowerCase()) ||
            item.artist.name
              .toLowerCase()
              .includes(props.search.toLowerCase())
    )
    // filter favourites by selected type, if all return all items
    .filter(
      item =>
        props.type === "all"
          ? true
          : item.type.toLowerCase() === props.type
    )
    // map each item in the list to the fourite component
    .map((item, index) => {
      return (
        <FavouritedItem
          key={index}
          type={item.album ? "Track" : "Album"}
          info={item}
        />
      );
    });

  // if the list is empty one of these divs will be used
  const emptyView =
    props.search !== "" || props.type !== "all" ? (
      <div>No results matching search query...</div>
    ) : (
      <div>No favourites have been added yet...</div>
    );

  // show the table of favourites if the user has favourites else one of the divs above
  const viewToShow =
    favouriteItems.length > 0 ? (
      <table className="bordered">
        <thead>
          <tr>
            <th>Name</th>
            <th></th>
            <th className="hide-on-small-only">Artist</th>
            <th>Type</th>
            <th />
          </tr>
        </thead>
        <tbody>{favouriteItems}</tbody>
      </table>
    ) : (
      emptyView
    );

  // return the rendered view
  return (
    <section>
      {(props.favourites.length > 0 ||
        props.search !== "" ||
        props.type !== "all") && (
        <FavouritesSearchForm
          inputChange={props.inputChange}
          search={props.search}
          type={props.type}
        />
      )}
      {viewToShow}
    </section>
  );
};

FavouritesList.propTypes = {
  search: PropTypes.string.isRequired,
  inputChange: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  favourites: PropTypes.array,
};

export default FavouritesList;
