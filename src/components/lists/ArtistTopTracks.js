import React from "react";
import PropTypes from "prop-types";
import TrackListItem from "./items/TrackListItem";

// Component for the top 5 tracks of an artist
const ArtistTopTracks = props => {
  let tracks = props.tracks.map((track, index) => {
    // Add positions and artist to the track object
    track["artist"] = props.artist;
    track["track_position"] = index + 1;
    return <TrackListItem key={index} track={track} artist={track["artist"]} />;
  });
  return (
    <section>
      <div className="card track-card">
        <table className="bordered">
          <thead>
            <tr>
              <th className="track-position">#</th>
              <th className="track-name">Name</th>
              <th />
              <th className="track-duration-heading">
                <i className="material-icons">schedule</i>
              </th>
              <th />
            </tr>
          </thead>
          <tbody>{tracks}</tbody>
        </table>
      </div>
    </section>
  );
};

// Typechecking
ArtistTopTracks.propTypes = {
  tracks: PropTypes.array.isRequired,
  artist: PropTypes.object.isRequired
};

export default ArtistTopTracks;
