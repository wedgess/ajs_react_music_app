import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { firebase } from "../../../firebase/";

/*
 * Component used for displaying album and track cards.
 * Used in Charts, ArtistInfo and Search Components
 */

// components initial state
const INITIAL_STATE = { showFavouritedIcon: false };

class CardListItem extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onItemClick = this.onItemClick.bind(this);
    this.onFavouriteClicked = this.onFavouriteClicked.bind(this);
    this.checkFavouritedItem = this.checkFavouritedItem.bind(this);
    this.state = { ...INITIAL_STATE };
  }

  onItemClick = event => {
    // need to declare here so it can be passed with history.push
    const prop = this.props.info;
    // use history to change the page passing the existing props
    this.props.history.push({
      pathname: `${this.props.rootPage}/${this.props.type}/${prop.title
        .split(" ")
        .join("")}`,
      state: {
        detail: prop
      }
    });
  };

  onFavouriteClicked = event => {
    // stop favourite event click propagating to the parent component
    event.stopPropagation();
    const { authUser } = this.context;
    const data = this.props.info;
    // this value is checked to change state on a favourited item
    data["favourite"] = !this.state.showFavouritedIcon;
    const obj = { [data.id]: JSON.stringify(data) };
    let favs = firebase.db.ref(`users/${authUser.uid}`).child("favourites");
    favs.update(obj);
    this.setState({ showFavouritedIcon: !this.state.showFavouritedIcon });
  };

  static contextTypes = {
    authUser: PropTypes.object,
    favourites: PropTypes.object
  };

  checkFavouritedItem = favourites => {
    if (favourites && favourites[this.props.info.id]) {
      const favourited = JSON.parse(favourites[this.props.info.id])[
        "favourite"
      ];
      // prevent recurssion, only set show favourite if it has changed
      if (this.state.showFavouritedIcon !== favourited) {
        this.setState({
          showFavouritedIcon: JSON.parse(favourites[this.props.info.id])[
            "favourite"
          ]
        });
      }
    }
  };

  componentWillMount() {
    const { favourites } = this.context;
    this.checkFavouritedItem(favourites);
  }

  componentDidUpdate() {
    const { favourites } = this.context;
    this.checkFavouritedItem(favourites);
  }

  render() {
    const { authUser } = this.context;

    const favIcon = this.state.showFavouritedIcon ? (
      <i className="material-icons" onClick={this.onFavouriteClicked}>
        favorite
      </i>
    ) : (
      <i className="material-icons" onClick={this.onFavouriteClicked}>
        favorite_border
      </i>
    );
    return (
      <div className="col l5ths m4 s6">
        <div className="card chart hoverable" onClick={this.onItemClick}>
          <div className="card-image">
            <div className="wrapper">
              <img
                className="responsive-img"
                alt="Cover"
                src={
                  this.props.info.album
                    ? this.props.info.album.cover_medium
                    : this.props.info.cover_medium
                }
              />
              {authUser && favIcon}
            </div>
          </div>
          <div className="card-content chart-card-content">
            <div className="album-name truncate"> {this.props.info.title}</div>{" "}
            <div className="chip truncate valign-wrapper">
              <img
                alt="Artist"
                src={
                  this.props.info.artist
                    ? this.props.info.artist.picture_small
                    : ""
                }
              />
              <span className="artist-name truncate">
                {this.props.info.artist ? this.props.info.artist.name : ""}
              </span>
              <div className="xplicit-language-container">
                {this.props.info.explicit_lyrics && (
                  <div className="xplicit-language-letter">E</div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// Typechecking
CardListItem.propTypes = {
  info: PropTypes.object.isRequired
};

// wrap with router to access props history to change route
export default withRouter(CardListItem);
