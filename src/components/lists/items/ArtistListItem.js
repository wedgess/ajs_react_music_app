import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

// component for artist cards shown in Search component
class ArtistListItem extends React.Component {
  onItemClick(event) {
    let prop = this.props.info;
    // url to redirect user to is artist with artist name cleaned up for url removing any spaces
    this.props.history.push({
      pathname: `/search/artist/${prop.name.split(" ").join("")}`,
      state: {
        detail: prop
      }
    });
  }

  render() {
    return (
      <div className="col l5ths m4 s6">
        <div
          onClick={this.onItemClick.bind(this)}
          className="card artist-card hoverable"
        >
          <div className="card-image">
            <img
              alt="Artist"
              className="responsive-img"
              src={this.props.info.picture_medium}
            />
          </div>
          <div className="card-content">
            <div className="artist-name truncate"> {this.props.info.name} </div>
          </div>
        </div>
      </div>
    );
  }
}

ArtistListItem.propTypes = {
  info: PropTypes.object.isRequired
};

export default withRouter(ArtistListItem);
