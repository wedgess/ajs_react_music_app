import React from "react";
import PropTypes from "prop-types";
import { Button, Icon } from "react-materialize";
import { withRouter } from "react-router-dom";
import { firebase } from "../../../firebase";

/*
This component is for displaying each of the users favourited items displayed in the users account page.
Handles deleting individual items from firebase database.
Item can be clicked on to display album or track info dependig on if item is a album or track.
*/

class FavouritedItem extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onFavouriteDelete = this.onFavouriteDelete.bind(this);
    this.onItemClick = this.onItemClick.bind(this);
  }

  // use provider pattern to get the authenticated user
  static contextTypes = {
    authUser: PropTypes.object
  };

  // on item click display the track or album info, type needs to be converted to lowercase and remove space from url
  onItemClick = event => {
    const prop = this.props.info;
    this.props.history.push({
      pathname: `/account/favourites/${this.props.type.toLowerCase()}/${this.props.info.title
        .split(" ")
        .join("")}`,
      state: {
        detail: prop
      }
    });
  };

  onFavouriteDelete = event => {
    // don't bubble event to parent
    event.stopPropagation();
    // get authenticated user from provider then delete users favourited item from FB DB
    const { authUser } = this.context;
    let favourite = firebase.db
      .ref(`users/${authUser.uid}`)
      .child("favourites")
      .child(`${this.props.info.id}`);
    favourite.remove();
    // show toast when item is deleted
    window.Materialize.toast(
      `Favourite: ${this.props.info.title} removed`,
      3000
    );
  };

  // render the table row for this favourite item
  render() {
    return (
      <tr onClick={this.onItemClick}>
        <td className="favourite-title-cell">
          <div className="favourite-img-title-container">
            {/* Album and track store the cover in different objects, therefore check if type is album */}
            <img
              alt="cover"
              className="responsive-img favourite-item-img hide-on-small-only"
              src={
                this.props.type === "Album"
                  ? this.props.info.cover_medium
                  : this.props.info.album.cover_medium
              }
            />
            <pre>
            <div className="favourite-title truncate">
              {this.props.info.title}
            </div>
            <div className="favourite-artist-name truncate hide-on-med-and-up show-on-small">
              {this.props.info.artist.name}
            </div>
            </pre>
          </div>
        </td>
        <td>
        {/* Only display E if album/track contains explicit lyrics */}
        <div className="favourite-explicit-language">
          {this.props.info.explicit_lyrics && (
            <div className="favourite-letter-container">E</div>
          )}
        </div>
        </td>
        <td className="favourite-artist-cell hide-on-small-only">
          <span className="favourite-artist-name truncate">
            {this.props.info.artist.name}
          </span>
        </td>
        <td className="favourite-type clickable-cell">{this.props.type}</td>
        <td>
          <Button
            floating
            className="white favourite-track-action small"
            onClick={this.onFavouriteDelete}
            waves="light"
          >
            <Icon>delete</Icon>
          </Button>
        </td>
      </tr>
    );
  }
}

FavouritedItem.propTypes = {
  info: PropTypes.object.isRequired
};

// use router to access props history to change route on item click
export default withRouter(FavouritedItem);
