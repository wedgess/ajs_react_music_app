import React from "react";
import PropTypes from "prop-types";
import { Modal, Button } from "react-materialize";
import ReactAudioPlayer from "react-audio-player";

/*
 * Used for album info to list each of the albums tracks.
 */

const INITIAL_STATE = {
  lyrics: ""
};

class TrackListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  componentWillMount() {
    // fetch lyrics from the API
    fetch(
      encodeURI(
        `https://api.lyrics.ovh/v1/${this.props.track.artist.name}/${
          this.props.track.title
        }`
      )
    )
      .then(response => {
        if (response.ok) return response.json();
        this.setState({
          lyrics: "No lyrics available..."
        });
        throw new Error(
          `Failed to get lyrics for ${this.props.track.artist.name} ${
            this.props.track.title
          }`
        );
      })
      .then(data => {
        this.setState({
          lyrics: data.lyrics.replace(/^(Paroles de la chanson).*/, "")
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    // console.log(this.props);
    return (
      <tr>
        <td className="track-position">{this.props.track.track_position}</td>
        <td className="track-title-cell">
          <span className="show-on-medium-and-up hide-on-small-only">
            {this.props.track.title}
          </span>
          <span className="show-on-small truncate hide-on-med-and-up">
            {this.props.track.title}
          </span>
        </td>
        <td className="track-item-xplicit-container">
          {this.props.track.explicit_lyrics && (
            <div className="track-item-xplicit-letter">E</div>
          )}
        </td>
        <td className="track-duration">
          {new Date(this.props.track.duration * 1000)
            .toISOString() // get time in minutes:seconds
            .substr(14, 5)}
        </td>
        <td className="track-actions-column">
          <Modal
            className="audio-modal"
            header={this.props.track.title}
            trigger={
              <Button
                floating
                className="material-icons white track-action small"
                waves="light"
                icon="play_arrow"
              />
            }
          >
            <ReactAudioPlayer src={this.props.track.preview} controls />
          </Modal>

          <Modal
            className="lyrics-modal"
            header={this.props.track.title}
            fixedFooter
            trigger={
              <Button
                floating
                className="material-icons white track-action small"
                waves="light"
                icon="library_books"
              />
            }
          >
            <pre>{this.state.lyrics}</pre>
          </Modal>
        </td>
      </tr>
    );
  }
}

// Typechecking
TrackListItem.propTypes = {
  track: PropTypes.object.isRequired,
  artist: PropTypes.object
};

export default TrackListItem;
