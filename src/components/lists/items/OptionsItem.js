import React from "react";
import PropTypes from "prop-types";

/*
 * Helper component for <select> elements. Used by both search and
 * FavouritesSearchForm components.
 */

const OptionsItem = props => (
  <option key={props.key} value={props.typeKey}>
    {props.typeValue}
  </option>
);

OptionsItem.propTypes = {
  typeKey: PropTypes.string.isRequired,
  typeValue: PropTypes.string.isRequired
};

export default OptionsItem;
