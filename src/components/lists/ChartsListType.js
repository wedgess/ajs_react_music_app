import React from "react";
import fetchJsonp from "fetch-jsonp";
import PropTypes from "prop-types";
import { CHARTS } from "../../constants/routes";
import CardListItem from "./items/CardListItem";

// initial component state
const INITIAL_STATE = {
  charts: []
};

class ChartsListType extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  componentWillMount() {
    // fetch chart items as JSONP (it is needed because of CORS)
    fetchJsonp(
      `https://api.deezer.com/chart/0/${this.props.type}s&output=jsonp`
    )
      .then(response => {
        if (response.ok) return response.json();
        throw new Error("Request failed");
      })
      .then(data => {
        this.setState({
          charts: data.data
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    let list = this.state.charts
      .sort((a, b) => a.position - b.position) // sort from 1 - 10
      .map((chartItem, index) => {
        return (
          <CardListItem
            key={chartItem.id}
            info={chartItem}
            type={this.props.type}
            rootPage={CHARTS}
          />
        );
      });
    return <section className="row"> {list} </section>;
  }
}

// Typechecking
ChartsListType.propTypes = {
  type: PropTypes.string.isRequired
};

export default ChartsListType;
