import React from "react";
import PropTypes from "prop-types";
import { firebase } from "../../firebase";

/*
 * Higher Order Component (HOC) for firebase uathentication and
 * users favourited items from FB DB.
 */

const INITIAL_STATE = {
  authUser: null,
  favourites: null
};

const withAuthentication = WrappedComponent => {
  class WithAuthentication extends React.Component {
    constructor(props) {
      super(props);

      this.state = { ...INITIAL_STATE };
    }

    getChildContext() {
      return {
        authUser: this.state.authUser,
        favourites: this.state.favourites
      };
    }

    componentDidMount() {
      // add FB listener for authentication change
      firebase.auth.onAuthStateChanged(authUser => {
        // when user is signed in
        if (authUser) {
          // get reference to user's favourites in FB DB
          let remoteFavs = firebase.db
            .ref(`users/${authUser.uid}`)
            .child("favourites");
          // add listener to remote to listen to db changes
          remoteFavs.on("value", snapshot => {
            let userFavourites = snapshot.val();
            // save favourites in state
            this.setState({ favourites: userFavourites });
          });
          // save authenticated user in state
          this.setState({ authUser: authUser });
        } else {
          // on sign out reset state
          this.setState({ ...INITIAL_STATE });
        }
      });
    }

    render() {
      return <WrappedComponent />;
    }
  }

  WithAuthentication.childContextTypes = {
    authUser: PropTypes.object,
    favourites: PropTypes.object
  };

  return WithAuthentication;
};

export default withAuthentication;
