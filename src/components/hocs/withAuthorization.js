import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { firebase } from "../../firebase";
import { SIGN_IN } from "../../constants/routes";

/*
 * HOC to ensure user can only access acount page if the user is
 * logged in, otherwise redirect to sign in component.
 */

const withAuthorization = authCondition => WrappedComponent => {
  class WithAuthorization extends React.Component {
    componentDidMount() {
      firebase.auth.onAuthStateChanged(authUser => {
        if (!authCondition(authUser)) {
          this.props.history.push(SIGN_IN);
        }
      });
    }

    render() {
      return this.context.authUser ? <WrappedComponent /> : null;
    }
  }

  WithAuthorization.contextTypes = {
    authUser: PropTypes.object
  };

  return withRouter(WithAuthorization);
};

export default withAuthorization;
