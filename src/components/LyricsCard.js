import React from "react";
import PropTypes from "prop-types";

const LyricsCard = props => (
  <div className="card">
    <div className="card-content">
      {/* Show lyrics preformatted so as to not remove any line breaks etc*/}
      <pre id="lyrics">{props.lyrics}</pre>
    </div>
  </div>
);

LyricsCard.propTypes = {
  lyrics: PropTypes.string.isRequired
};

export default LyricsCard;
