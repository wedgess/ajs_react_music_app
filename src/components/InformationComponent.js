import React from "react";
import PropTypes from "prop-types";
import { Row } from "react-materialize";

const InformationComponent = props => (
  <Row className="valign-wrapper">
    <img
      className="info-cover responsive-img col l2"
      alt="Album cover"
      src={props.cover}
    />
    <div className="col l10">
      <div className="inline-info-container">
        <h4 className="info-inline-title info-title">{props.title}</h4>
        <div className="info-xplicit-container info-inline-text">
          {props.explicit_lyrics && (
            <div className="info-xplicit-letter">E</div>
          )}
        </div>
      </div>

      <div className="valign-wrapper">
        <img
          className="info-artist-img responsive-img circle"
          alt="Artist"
          src={props.artist_img}
        />
        <span className="info-artist-name">{props.artist_name}</span>
      </div>
      <div className="inline-info-container">
        {props.release_date && (
          <span className="info-inline-text">
            <i className="material-icons">album</i>
            {props.release_date}
          </span>
        )}
        {props.track_count && (
          <span className="info-inline-text">
            <i className="material-icons">audiotrack</i>
            {props.track_count}
          </span>
        )}
        <span className="info-inline-text">
          <i className="material-icons">schedule</i>
          {new Date(props.track_time * 1000).toISOString().substr(11, 8)}
        </span>
      </div>
    </div>
  </Row>
);

InformationComponent.propTypes = {
  cover: PropTypes.string.isRequired,
  artist_img: PropTypes.string.isRequired,
  track_time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  release_date: PropTypes.string,
  explicit_lyrics: PropTypes.bool,
};

export default InformationComponent;
