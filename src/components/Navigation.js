import React from "react";
import { Navbar, NavItem } from "react-materialize";
import { LinkContainer } from "react-router-bootstrap";
import PropTypes from "prop-types";
import SignOutButton from "./pages/SignOut";
import { CHARTS, SEARCH, PROFILE, SIGN_IN } from "../constants/routes";

/*
 * Navigation component will use one of Auth or NonAuth navigation depending
 * on the users login status to FireBase.
 */

// Logged in navigation
const NavigationAuth = () => (
  <Navbar
    options={{ closeOnClick: true, draggable: true }}
    brand="Music Lyrics"
    right
  >
    <LinkContainer to={CHARTS}>
      <NavItem>Charts</NavItem>
    </LinkContainer>
    <LinkContainer to={SEARCH}>
      <NavItem>Search</NavItem>
    </LinkContainer>
    <LinkContainer to={PROFILE}>
      <NavItem>Profile</NavItem>
    </LinkContainer>
    <SignOutButton />
  </Navbar>
);

// Not logged in navigation
const NavigationNonAuth = () => (
  <Navbar
    options={{ closeOnClick: true, draggable: true }}
    brand="Music Lyrics"
    right
  >
    <LinkContainer to={CHARTS}>
      <NavItem>Charts</NavItem>
    </LinkContainer>
    <LinkContainer to={SEARCH}>
      <NavItem>Search</NavItem>
    </LinkContainer>
    <LinkContainer to={SIGN_IN}>
      <NavItem>Sign in</NavItem>
    </LinkContainer>
  </Navbar>
);

class Navigation extends React.Component {
  // get auth user from provider
  static contextTypes = {
    authUser: PropTypes.object
  };

  // render navbar based on if user is signed in or not
  render() {
    const { authUser } = this.context;
    return <div>{authUser ? <NavigationAuth /> : <NavigationNonAuth />}</div>;
  }
}

export default Navigation;
