import React from "react";
import { Button, Input, Row } from "react-materialize";
import { auth } from "../../firebase";
import "../../sass/fb-login-common.css";

/*
 * PasswordChangeForm component for changing users FB password.
 */

// Components initial state
const INITIAL_STATE = {
  passwordOne: "",
  passwordTwo: "",
  error: null
};

class PasswordChangeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  // change state based on user input
  handleInputChange = event => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({ [name]: value });
  }

  onSubmit = event => {
    // update password in FB
    auth
      .doPasswordUpdate(this.state.passwordOne)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
      })
      .catch(error => {
        this.setState({ error: error });
      });

    event.preventDefault();
  };

  render() {
    // get items from state
    const { passwordOne, passwordTwo, error } = this.state;
    // validation that passwords match and aren't empty (used for submit button)
    const isInvalid = passwordOne !== passwordTwo || passwordOne === "";

    return (
      <form onSubmit={this.onSubmit}>
        <Row>
          <Input
            value={passwordOne}
            onChange={this.handleInputChange}
            s={12}
            type="password"
            name="passwordOne"
            label="New Password"
          />
        </Row>
        <Row>
          <Input
            value={passwordTwo}
            onChange={this.handleInputChange}
            s={12}
            name="passwordTwo"
            type="password"
            label="Confirm New Password"
          />
        </Row>
        <Row>
          <Button className="input-btn" disabled={isInvalid} type="submit">
            Reset Password
          </Button>
        </Row>
        <Row>{error && <p className="error-text">{error.message}</p>}</Row>
      </form>
    );
  }
}

export default PasswordChangeForm;
