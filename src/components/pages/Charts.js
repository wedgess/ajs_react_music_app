import React from "react";
import ChartsListType from "../lists/ChartsListType";
import "../../sass/charts.css";

// Charts Page component
const ChartsPage = () => (
  <div className="container">
  <h4>Chart Albums</h4>
    <ChartsListType type="album" />
    <h4 id="chart-tracks-title">Chart Tracks</h4>
    <ChartsListType type="track" />
  </div>
);

export default ChartsPage;
