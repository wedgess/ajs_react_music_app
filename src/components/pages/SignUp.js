import React from "react";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import { Input, Button, Row } from "react-materialize";
import { auth, db } from "../../firebase";
import { SIGN_UP, LANDING } from "../../constants/routes";
import "../../sass/fb-login-common.css";

/*
 * Contains the sign up page, the sign up form and sign up link components.
 */

const SignUpPage = props => (
  <div className="container">
    <div className="card">
      <div className="card-content">
        <h4>Sign Up</h4>
        <SignUpForm history={props.history} />
      </div>
    </div>
  </div>
);

SignUpPage.propTypes = {
  history: PropTypes.object.isRequired
};

// SignUpForm components intial state
const INITIAL_STATE = {
  username: "",
  email: "",
  passwordOne: "",
  passwordTwo: "",
  favourites: {},
  error: null
};

// form component
class SignUpForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  // change state based on user input
  handleInputChange = event => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({ [name]: value });
  }

  onSubmit = event => {
    // get items from state
    const { username, email, passwordOne, favourites } = this.state;

    // sign user up with FB auth
    auth
      .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // create user on FB
        db
          .doCreateUser(authUser.uid, username, email, favourites)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
            // return user to landing page when signed up
            this.props.history.push(LANDING);
          })
          .catch(error => {
            this.setState({error: error});
          });
        // update user profile to show display name/ username
        authUser.updateProfile({ displayName: username });
      })
      .catch(error => {
        this.setState({error: error});
      });

    event.preventDefault();
  };

  render() {
    const { username, email, passwordOne, passwordTwo, error } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === "" ||
      username === "" ||
      email === "";

    return (
      <form onSubmit={this.onSubmit}>
        <Row>
          <Input
            value={username}
            onChange={this.handleInputChange}
            s={12}
            type="text"
            label="Username"
            name="username"
          />
        </Row>
        <Row>
          <Input
            value={email}
            onChange={this.handleInputChange}
            s={12}
            type="text"
            name="email"
            label="Email Address"
          />
        </Row>
        <Row>
          <Input
            value={passwordOne}
            onChange={this.handleInputChange}
            s={12}
            type="password"
            name="passwordOne"
            label="Password"
          />
        </Row>
        <Row>
          <Input
            value={passwordTwo}
            onChange={this.handleInputChange}
            s={12}
            type="password"
            name="passwordTwo"
            label="Confirm Password"
          />
        </Row>
        <Row>
          <Button className="input-btn" disabled={isInvalid} type="submit">
            Sign Up
          </Button>
        </Row>
        <Row>{error && <p>{error.message}</p>}</Row>
      </form>
    );
  }
}

// signup link component used in sign in form
const SignUpLink = () => (
  <p>
    No account? <Link to={SIGN_UP}>Sign Up</Link>
  </p>
);

// must use with router to access props history needed for redirect to home page on sign up
export default withRouter(SignUpPage);

// link used in sign in component
export { SignUpForm, SignUpLink };
