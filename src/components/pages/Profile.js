import React from "react";
import PropTypes from "prop-types";
import { Collapsible, CollapsibleItem } from "react-materialize";
import PasswordChangeForm from "./PasswordChange";
import withAuthorization from "../hocs/withAuthorization";
import FavouritesList from "../lists/FavouritesList";
import "../../sass/account.css";

// ProfilePage components initial state
const INITIAL_STATE = {
  search: "",
  type: "all"
};

class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  // state from provider
  static contextTypes = {
    authUser: PropTypes.object,
    favourites: PropTypes.object
  };

  // update state when input values change
  handleInputChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
  };

  render() {
    // get favourites and authUser from provider
    const { authUser, favourites } = this.context;
    const favouritesItems = [];
    if (favourites) {
      Object.keys(favourites).forEach(item => {
        favouritesItems.push(JSON.parse(favourites[item]));
      });
    }
    return (
      <div className="container">
        <Collapsible popout defaultActiveKey={favouritesItems.length > 0 && 2}>
          <CollapsibleItem header="Account Information" icon="person">
            <h6 className="account-info-title">Username</h6>
            <p className="account-info-value">{authUser.displayName}</p>
            <h6 className="account-info-title">Registered email</h6>
            <p className="account-info-value">{authUser.email}</p>
          </CollapsibleItem>
          <CollapsibleItem header="Password Reset" icon="lock">
            <PasswordChangeForm />
          </CollapsibleItem>
          <CollapsibleItem header="Favourites" icon="favorite">
            <FavouritesList
              inputChange={this.handleInputChange}
              favourites={favouritesItems}
              search={this.state.search}
              type={this.state.type}
            />
          </CollapsibleItem>
        </Collapsible>
      </div>
    );
  }
}

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(ProfilePage);
