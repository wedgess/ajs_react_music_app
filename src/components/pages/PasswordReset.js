import React from "react";
import { Link } from "react-router-dom";
import { Button, Input, Row } from "react-materialize";
import { auth } from "../../firebase";
import { PASSWORD_RESET } from "../../constants/routes";
import "../../sass/fb-login-common.css";

/*
 * Contains the PasswordResetPage and Form component.
 */

const PasswordResetPage = () => (
  <div className="container">
    <div className="card">
      <div className="card-content">
        <h4>Password Reset</h4>
        <PasswordResetForm />
      </div>
    </div>
  </div>
);

// Form components initial state
const INITIAL_STATE = {
  email: "",
  error: null
};

class PasswordResetForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  // change state based on user input
  handleInputChange = event => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({ [name]: value });
  };

  onSubmit = event => {
    // reset password on firebase
    auth
      .doPasswordReset(this.state.email)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
      })
      .catch(error => {
        this.setState({ error: error });
      });

    // stop browser refresh
    event.preventDefault();
  };

  render() {
    const { email, error } = this.state;

    // ensure email is not empty
    const isInvalid = email === "";

    return (
      <form onSubmit={this.onSubmit}>
        <Row>
          <Input
            s={12}
            value={this.state.email}
            onChange={this.handleInputChange}
            type="text"
            name="email"
            label="Email Address"
          />
        </Row>
        <Row>
          <Button className="input-btn" disabled={isInvalid} type="submit">
            Reset Password
          </Button>
        </Row>

        {error && <p>{error.message}</p>}
      </form>
    );
  }
}

// password reset link for signin component
const PasswordResetLink = () => (
  <p>
    <Link to={PASSWORD_RESET}>Forgotten your Password?</Link>
  </p>
);

export default PasswordResetPage;

// export the form and reset link to be used in SingIn component
export { PasswordResetForm, PasswordResetLink };
