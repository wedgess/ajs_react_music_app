import React from "react";
import fetchJsonp from "fetch-jsonp";
import TracksList from "../lists/TracksList";
import PropTypes from "prop-types";
import InformationComponent from "../InformationComponent";
import { withRouter } from "react-router-dom";
import "../../sass/album-info.css";

// AlbumInfo components initial state
const INITIAL_STATE = {
  tracks: [],
  trackCount: 0
};

class AlbumInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  componentWillMount() {
    // fetch JSONP needs to be used because of CORS
    fetchJsonp(`${this.props.location.state.detail.tracklist}&output=jsonp`)
      .then(response => {
        if (response.ok) return response.json();
        throw new Error("Request failed");
      })
      .then(data => {
        this.setState({
          tracks: data.data,
          trackCount: data.data.length
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    // add up total duration of all album tracks
    let trackTime = 0;
    this.state.tracks.forEach(track => (trackTime += track.duration));

    return (
      <div className="container">
        <InformationComponent
          cover={this.props.location.state.detail.cover_medium}
          title={this.props.location.state.detail.title}
          artist_img={this.props.location.state.detail.artist.picture_small}
          artist_name={this.props.location.state.detail.artist.name}
          track_count={this.state.trackCount}
          track_time={trackTime}
          explicit_lyrics={this.props.location.state.detail.explicit_lyrics}
          release_date={this.props.location.state.detail.release_date}
        />
        <TracksList
          artistName={this.props.location.state.detail.artist_name}
          tracks={this.state.tracks}
        />
      </div>
    );
  }
}

AlbumInfo.propTypes = {
  location: PropTypes.object.isRequired
};

export default withRouter(AlbumInfo);
