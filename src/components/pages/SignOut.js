import React from "react";
import { Button } from "react-materialize";
import { auth } from "../../firebase";

// SignOutButton component
const SignOutButton = () => (
  <Button id="sign-out-btn" onClick={auth.doSignOut}>
    Sign Out
  </Button>
);

export default SignOutButton;
