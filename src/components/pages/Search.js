import React from "react";
import { Row, Input, Button } from "react-materialize";
import fetchJsonp from "fetch-jsonp";
import PropTypes from "prop-types";
import ArtistListItem from "../lists/items/ArtistListItem";
import CardListItem from "../lists/items/CardListItem";
import OptionsItem from "../lists/items/OptionsItem";
import * as Store from "../../Store";
import { SEARCH } from "../../constants/routes";
import "../../sass/search.css";

// different search types for select options
const searchTypes = {
  artist: "Artist",
  album: "Album",
  track: "Track"
};

// search page component
const SearchPage = () => (
  <div className="container">
    <div className="card card-search">
      <div className="card-content">
        <Row>
          <h4>Search</h4>
        </Row>
        <SearchForm />
      </div>
    </div>
  </div>
);

// component which contains the search results
const SearchResultContainer = props => (
  <div>
    <h4>Results</h4>
    <Row>{props.results}</Row>
  </div>
);

SearchResultContainer.propTypes = {
  results: PropTypes.array
};

// initial state for search form component
const INITIAL_STATE = {
  searchResults: [],
  searchType: "artist",
  query: ""
};

class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    // check saved state and if present then set current state
    var state = Store.getSearchList();
    if (state) {
      this.setState(state);
    }
  }

  componentWillUnmount() {
    // save state when redirecting to new page, on browser back this state is restored
    Store.saveSearchList(this.state);
  }

  handleInputChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    // reset search results on search type change
    if (name === "searchType") {
      this.setState({ searchResults: [] });
    }

    this.setState({ [name]: value });
  };

  onSubmit = event => {
    fetchJsonp(
      encodeURI(
        `https://api.deezer.com/search/${this.state.searchType}?q=${
          this.state.query
        }&output=jsonp`
      )
    )
      .then(response => {
        if (response.ok) return response.json();
        throw new Error("Request failed");
      })
      .then(data => {
        this.setState({
          searchResults: data.data
        });
      })
      .catch(error => {
        console.log(error);
      });

    event.preventDefault();
  };

  buildResultsList() {
    let results = [];
    const keys = Object.keys(searchTypes);
    switch (this.state.searchType) {
      default:
      case keys[0]:
        results = this.state.searchResults.map((result, index) => {
          return <ArtistListItem key={index} info={result} />;
        });
        break;
      case keys[1]:
        results = this.state.searchResults.map((result, index) => {
          return (
            <CardListItem
              key={index}
              info={result}
              type={keys[1]}
              rootPage={SEARCH}
            />
          );
        });
        break;
      case keys[2]:
        results = this.state.searchResults.map((result, index) => {
          return (
            <CardListItem
              key={index}
              info={result}
              type={keys[2]}
              rootPage={SEARCH}
            />
          );
        });
        break;
    }

    return results;
  }

  render() {
    const searchResults = this.buildResultsList();

    let searchOptions = Object.keys(searchTypes).map((key, index) => {
      return (
        <OptionsItem key={index} typeKey={key} typeValue={searchTypes[key]} />
      );
    });

    const { query } = this.state;
    const isInvalid = query === "";

    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <Row>
            <Input
              name="query"
              m={6}
              s={12}
              label="Query"
              value={this.state.query}
              onChange={this.handleInputChange}
            />
            <Input
              name="searchType"
              s={12}
              m={6}
              type="select"
              label="Type"
              value={this.state.searchType}
              onChange={this.handleInputChange}
            >
              {searchOptions}
            </Input>
          </Row>
          <Row>
            <Button
              className="input-btn"
              disabled={isInvalid}
              waves="light"
              type="submit"
            >
              Search
            </Button>
          </Row>
        </form>
        {searchResults.length > 0 && <SearchResultContainer results={searchResults} />}
      </div>
    );
  }
}

export default SearchPage;
