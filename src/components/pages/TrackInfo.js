import React from "react";
import PropTypes from "prop-types";
import ReactAudioPlayer from "react-audio-player";
import { withRouter } from "react-router-dom";
import InformationComponent from "../InformationComponent";
import LyricsCard from "../LyricsCard";
import "../../sass/track-info.css";

/*
 * TrackInfo page and item components users for charts track items.
 */

const TrackInfoPage = props => (
  <div className="container">
    <TrackInfoDetails info={props.location.state.detail} />
  </div>
);

// Typechecking
TrackInfoPage.propTypes = {
  location: PropTypes.object.isRequired
};

// TrackInfoDetails components initial state
const INITIAL_STATE = {
  lyrics: ""
};

class TrackInfoDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  componentWillMount() {
    // fetch normally for lyrics as it is using a different API that has no CORS issue
    fetch(
      encodeURI(
        `https://api.lyrics.ovh/v1/${this.props.info.artist.name}/${
          this.props.info.title
        }`
      )
    )
      .then(response => {
        if (response.ok) return response.json();
        this.setState({
          lyrics: "No lyrics available..."
        });
        throw new Error(
          `Failed to get lyrics for ${this.props.track.artist.name} ${
            this.props.track.title
          }`
        );
      })
      .then(data => {
        // lyrics can contain a string with French, so remove the French and anything on same line if exists
        this.setState({
          lyrics: data.lyrics.replace(/^(Paroles de la chanson).*/, "")
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <InformationComponent
          cover={this.props.info.album.cover_medium}
          title={this.props.info.title}
          artist_img={this.props.info.artist.picture_small}
          artist_name={this.props.info.artist.name}
          track_time={this.props.info.duration}
          explicit_lyrics={this.props.info.explicit_lyrics}
        />
        {/* Show only on mobile devices to sit under image and info */}
        <ReactAudioPlayer src={this.props.info.preview} controls />
        <LyricsCard lyrics={this.state.lyrics} />
      </div>
    );
  }
}

// Typechecking
TrackInfoDetails.propTypes = {
  info: PropTypes.object.isRequired
};

// need router to access track info
export default withRouter(TrackInfoPage);
