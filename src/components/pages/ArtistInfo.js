import React from "react";
import { Row } from "react-materialize";
import fetchJsonp from "fetch-jsonp";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import CardListItem from "../lists/items/CardListItem"
import ArtistTopTracks from "../lists/ArtistTopTracks";
import { SEARCH } from "../../constants/routes";
import "../../sass/artist-info.css";

// artist info page component
const ArtistInfoPage = props => (
  <div className="container">
    <ArtistInfo info={props.location.state.detail} />
  </div>
);

// intitial state for ArtistInfo component
const INITIAL_STATE = {
  tracks: [],
  albums: [],
  totalAlbums: 0
};

// main artist info component
class ArtistInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  componentDidMount() {
    // fetch JSONP needs to be used because of CORS
    // fetch atrists top tracks
    fetchJsonp(
      `https://api.deezer.com/artist/${this.props.info.id}/top&output=jsonp`
    )
      .then(response => {
        if (response.ok) return response.json();
        throw new Error("Request failed");
      })
      .then(data => {
        // console.log(data.data);
        this.setState({
          tracks: data.data
        });
      })
      .catch(error => {
        console.log(error);
      });
    // fetch artists albums
    fetchJsonp(
      `https://api.deezer.com/artist/${this.props.info.id}/albums&output=jsonp`
    )
      .then(response => {
        if (response.ok) return response.json();
        throw new Error("Request failed");
      })
      .then(data => {
        // console.log(data.data);
        this.setState({
          albums: data.data,
          totalAlbums: data.total
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    let albums = this.state.albums.map((album, index) => {
      // need to manually add artist info as it is not contained in the album results from query
      album["artist"] = this.props.info;
      return (
        <CardListItem
          key={index}
          info={album}
          type={`album`}
          rootPage={`${SEARCH}/artist/${this.props.info.name}`.replace(" ", "")}
        />
      );
    });

    return (
      <div>
        <Row className="valign-wrapper">
          <img
            alt=""
            className="responsive-img artist-img circle"
            src={this.props.info.picture_medium}
          />
          <div className="artist-main-info">
            <h4>{this.props.info.name}</h4>
            <div>{`Albums: ${this.state.totalAlbums}`}</div>
          </div>
        </Row>
        <Row>
          <h5 className="artist-top-title">Top Tracks</h5>
        </Row>
        <ArtistTopTracks artist={this.props.info} tracks={this.state.tracks} />
        <Row>
          <h5 className="artist-top-title">Albums</h5>
        </Row>
        <Row>{albums}</Row>
      </div>
    );
  }
}

ArtistInfoPage.propTypes = {
  info: PropTypes.object
};

export default withRouter(ArtistInfoPage);
