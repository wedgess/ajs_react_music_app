import React from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { Button, Input, Row } from "react-materialize";
import { SignUpLink } from "./SignUp";
import { PasswordResetLink } from "./PasswordReset";
import { auth } from "../../firebase";
import { PROFILE } from "../../constants/routes";
import "../../sass/fb-login-common.css";

/*
 * Sigin in page and form component for FB.
 */

// Page component
const SignInPage = props => (
  <div className="container">
    <div className="card">
      <div className="card-content">
        <h4>Sign In</h4>
        <SignInForm history={props.history} />
        <PasswordResetLink />
        <SignUpLink />
      </div>
    </div>
  </div>
);

SignInPage.propTypes = {
  history: PropTypes.object.isRequired
};

// SignInForm components intial state
const INITIAL_STATE = {
  email: "",
  password: "",
  error: null
};

class SignInForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  // change state based on user input
  handleInputChange = event => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    this.setState({ [name]: value });
  };

  // submit listener for form submit
  onSubmit = event => {
    const { email, password } = this.state;
    const { history } = this.props;

    auth
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        // go to account page when successfully signed in
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(PROFILE);
      })
      .catch(error => {
        this.setState({ error: error });
      });

    event.preventDefault();
  };

  render() {
    // get values from state
    const { email, password, error } = this.state;

    // ensure password and email aren't empty
    const isInvalid = password === "" || email === "";

    return (
      <form onSubmit={this.onSubmit}>
        <Row>
          <Input
            value={email}
            onChange={this.handleInputChange}
            s={12}
            type="text"
            name="email"
            label="Email Address"
          />
        </Row>
        <Row>
          <Input
            value={password}
            onChange={this.handleInputChange}
            s={12}
            type="password"
            name="password"
            label="Password"
          />
        </Row>
        <Row>
          <Button className="input-btn" disabled={isInvalid} type="submit">
            Sign In
          </Button>
        </Row>
        <Row>{error && <p className="error-text col s12">{error.message}</p>}</Row>
      </form>
    );
  }
}

// use withRouter to access history and pass to sign in form for redirect
export default withRouter(SignInPage);

export { SignInForm };
