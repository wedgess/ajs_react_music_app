import * as firebase from "firebase";

var config = {
  apiKey: "AIzaSyDGnFsVJR3l7xt2PNezER3hx5zh02kNZGs",
  authDomain: "react-music-app.firebaseapp.com",
  databaseURL: "https://react-music-app.firebaseio.com",
  projectId: "react-music-app",
  storageBucket: "",
  messagingSenderId: "963173758746"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export { db, auth };
