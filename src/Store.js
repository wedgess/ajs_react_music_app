// Store for saving and retrieving state
export const savedState = {};

export function saveSearchList(state) {
  savedState["searchResults"] = state;
}

export function getSearchList() {
  return savedState["searchResults"];
}
